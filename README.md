# Flask project example by inlanger

This is a test project showing Flask basic features. I use it for URLs and form handling. [TinyDB](https://pypi.python.org/pypi/tinydb) for data storing. Twitter bootstrap used for front-end.