from tinydb import TinyDB, where

from snippler import settings


class Snippet:

    def __init__(self):
        self.db = TinyDB(settings.DB_PATH)

    def save(self, data):
        return self.db.insert(data)

    def get(self, slug):
        return self.db.get(where('slug') == slug)

    def all(self):
        return self.db.all()

snippet = Snippet()
