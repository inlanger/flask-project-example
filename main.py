import datetime

from flask import Flask, flash, redirect, request, render_template, abort

from snippler import settings
from snippler.data import snippet
from snippler.utils import slugify

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/list/')
def snippets_list():
    return render_template('snippets/list.html', snippets=snippet.all())


@app.route('/snippet/<slug>/')
def single(slug):
    single_snippet = snippet.get(slug=slug)
    if not single_snippet:
        return abort(404)
    return render_template('snippets/single.html', snippet=single_snippet)


@app.route('/create/', methods=['POST'])
def create():
    if request.form['snippet']:

        data = {
            'title': request.form['title'],
            'snippet': request.form['snippet'],
            'slug': slugify(request.form['title']),
            'datetime': datetime.datetime.now().isoformat()
        }

        snippet.save(data)
        flash('Snippet created!', 'info')
    else:
        flash('No data for saving!', 'warning')

    return redirect('/list/')


@app.route('/<template_name>/')
def page(template_name):
    return render_template('pages/{}.html'.format(template_name))


if __name__ == '__main__':
    app.secret_key = settings.SECRET_KEY
    app.run(debug=True)
